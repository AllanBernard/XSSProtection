package ee.bcs.koolitus;

public class Main {

	public static void main(String[] args) {
		
		String malicious = "<script>alert('Annoying message!')</script>";
		String escaped = Utils.escapeHtml(malicious);
		
		System.out.println("This is malicious input entered by user:");
		System.out.println(malicious);
		System.out.println();
		System.out.println("This is disarmed output:");
		System.out.println(escaped);
		System.out.println();
		System.out.println("A web browser will convert it and display the initial input but will not run it as a script.");
	}

}
