package ee.bcs.koolitus;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;

public class Utils {

	public static final HashMap<Integer, String> HTML_ENTITIES = new HashMap<Integer, String>();
	static {
		HTML_ENTITIES.put(34, "&quot;");
		HTML_ENTITIES.put(35, "&num;");
		HTML_ENTITIES.put(36, "&dollar;");
		HTML_ENTITIES.put(37, "&percnt;");
		HTML_ENTITIES.put(38, "&amp;");
		HTML_ENTITIES.put(39, "&apos;");
		HTML_ENTITIES.put(59, "&semi;");
		HTML_ENTITIES.put(60, "&lt;");
		HTML_ENTITIES.put(61, "&equals;");
		HTML_ENTITIES.put(62, "&gt;");
		HTML_ENTITIES.put(123, "&lcub;");
		HTML_ENTITIES.put(124, "&verbar;");
		HTML_ENTITIES.put(125, "&rcub;");
		HTML_ENTITIES.put(160, "&nbsp;");
		HTML_ENTITIES.put(167, "&sect;");
	}

	public static String escapeHtml(String str) {
		if(str == null || str.length() == 0) {return str;}
		try {
			StringWriter writer = new StringWriter((int) (str.length() * 1.5));
			escape(writer, str);
			return writer.toString();
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return null;
		}
	}

	private static void escape(Writer writer, String str) throws IOException {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			int ascii = (int) c;
			String entityName = (String) HTML_ENTITIES.get(ascii);
			if (entityName == null) {
				if (c > 0x7F) {
					writer.write("&#");
					writer.write(Integer.toString(c, 10));
					writer.write(';');
				} else {
					writer.write(c);
				}
			} else {
				writer.write(entityName);
			}
		}
	}	
	
	
}
